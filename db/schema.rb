# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170330133213) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "coins", force: :cascade do |t|
    t.string   "coin_name"
    t.string   "cat_number"
    t.string   "nominal"
    t.date     "release_date"
    t.integer  "series_id"
    t.text     "coin_desc"
    t.string   "avers"
    t.string   "revers"
    t.string   "metal"
    t.integer  "circulation"
    t.boolean  "published"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["series_id"], name: "index_coins_on_series_id", using: :btree
  end

  create_table "series", force: :cascade do |t|
    t.string   "series_name"
    t.boolean  "published"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "user_coins", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "coin_id"
    t.boolean  "public",                              default: true
    t.boolean  "buy",                                 default: false
    t.boolean  "sell",                                default: false
    t.boolean  "seek",                                default: false
    t.boolean  "offer",                               default: false
    t.decimal  "price",      precision: 10, scale: 2
    t.string   "access_key"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.index ["coin_id"], name: "index_user_coins_on_coin_id", using: :btree
    t.index ["user_id"], name: "index_user_coins_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin_role",             default: false
    t.boolean  "guest_role",             default: true
    t.string   "username"
    t.boolean  "user_role"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  add_foreign_key "coins", "series"
  add_foreign_key "user_coins", "coins"
  add_foreign_key "user_coins", "users"
end
