class Coins < ActiveRecord::Migration[5.0]
  def change
    create_table :coins do |t|
      t.string :coin_name
      t.string :cat_number
      t.string :nominal
      t.date :release_date
      t.references :series, foreign_key: true
      t.text :coin_desc
      t.string :avers
      t.string :revers
      t.string :metal
      t.integer :circulation
      t.boolean :published
      t.timestamps
    end
  end
end
