class RenameRoleColumnInUser < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :user_role, :guest_role
    add_column :users, :user_role, :boolean
  end
end
