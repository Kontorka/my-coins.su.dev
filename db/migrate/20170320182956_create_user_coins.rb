class CreateUserCoins < ActiveRecord::Migration[5.0]
  def change
    create_table :user_coins do |t|
      t.references :user, foreign_key: true
      t.references :coin, foreign_key: true
      t.boolean :public, default: true
      t.boolean :buy, default: false
      t.boolean :sell, default: false
      t.boolean :seek, default: false
      t.boolean :offer, default: false
      t.decimal :price, :precision => 10, :scale => 2
      t.string :access_key

      t.timestamps
    end
  end
end
