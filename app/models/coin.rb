class Coin < ApplicationRecord
  mount_uploader :avers, CoinsUploader
  mount_uploader :revers, CoinsUploader

  belongs_to :series
end
