class SeriesController < ApplicationController
  before_action :set_series, only: [:show, :edit, :update, :destroy]

  require 'open-uri'
  require 'nokogiri'
  require 'net/http'

  def parse

    @qwe = []

    (1993..2017).each do |i|

      doc = File.open(File.absolute_path('public/parser/'+i.to_s+'.htm'))

      metal = nil
      circulation = nil
      quality = nil
      diameter = nil
      thickness = nil
      weight = nil
      avers = nil
      revers = nil

      html = Nokogiri::HTML(doc)
      html.css('tr').each do |data|

        if data.css('td b').first
          s_name = data.css('td b').text.split(': ')[1]

          if(s_name == nil)
            s_name = 'Без серии'
          end

          series = Series.find_or_create_by(series_name: s_name)
          @series_id = series.id
          series.published = true
          series.save
        end

        if(@series_id == nil)
          s_name = 'Без серии'
          series = Series.find_or_create_by(series_name: s_name)
          @series_id = series.id
          series.published = true
          series.save
        end

        if(data.css('td')[1] != nil)
          release_date = data.css('td')[0].text
          cat_number = data.css('td')[1].text
          nominal = data.css('td')[2].css('font').text
          name = data.css('td')[3].css('a').text

          uri = URI.parse('http://cbr.ru/bank-notes_coins/Base_of_memorable_coins/ShowCoins.aspx?cat_num='+cat_number)
          http = Net::HTTP.new(uri.host, uri.port)
          request = Net::HTTP::Get.new(uri.request_uri)
          response = http.request(request)
          html = Nokogiri::HTML(response.body)

          html.search('br').each do |n|
            n.replace("\n")
          end

          if(html.css('.infobox tr')[2])
            metal = html.css('.infobox tr')[2].css('td')[1].text
          end
          if(html.css('.infobox tr')[7])
            circulation = html.css('.infobox tr')[7].css('td')[1].text
          end
          if(html.css('.infobox tr')[1])
            quality = html.css('.infobox tr')[1].css('td')[1].text
          end
          if(html.css('.infobox tr')[5])
            diameter = html.css('.infobox tr')[5].css('td')[1].text
          end
          if(html.css('.infobox tr')[6])
            thickness = html.css('.infobox tr')[6].css('td')[1].text
          end
          if(html.css('.infobox tr')[3])
            weight = html.css('.infobox tr')[3].css('td')[1].text
          end

          Coin.find_or_create_by(cat_number: cat_number) do |coin|
            coin.release_date = release_date
            coin.nominal = nominal
            coin.series_id = @series_id
            coin.coin_name = name

            coin.metal = metal
            coin.circulation = circulation

            coin.remote_avers_url = 'http://cbr.ru/today/PhotoStore/img/'+cat_number+'.jpg'
            coin.remote_revers_url = 'http://cbr.ru/today/PhotoStore/img/'+cat_number+'r.jpg'

            coin.save
          end

        end

      end
    end
  end


  # GET /series
  # GET /series.json
  def index
    @series = Series.all
    @coins = Series.first.coins.page params[:page]
  end

  # GET /series/1
  # GET /series/1.json
  def show
    @all_series = Series.all
    @coins = @series.coins.page params[:page]
  end

  # GET /series/new
  def new
    @series = Series.new
  end

  # GET /series/1/edit
  def edit
  end

  # POST /series
  # POST /series.json
  def create
    @series = Series.new(series_params)

    respond_to do |format|
      if @series.save
        format.html { redirect_to @series, notice: 'Series was successfully created.' }
        format.json { render :show, status: :created, location: @series }
      else
        format.html { render :new }
        format.json { render json: @series.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /series/1
  # PATCH/PUT /series/1.json
  def update
    respond_to do |format|
      if @series.update(series_params)
        format.html { redirect_to @series, notice: 'Series was successfully updated.' }
        format.json { render :show, status: :ok, location: @series }
      else
        format.html { render :edit }
        format.json { render json: @series.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /series/1
  # DELETE /series/1.json
  def destroy
    @series.destroy
    respond_to do |format|
      format.html { redirect_to series_index_url, notice: 'Series was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_series
      @series = Series.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def series_params
      params.require(:series).permit(:series_name, :published)
    end
end
