class HomeController < ApplicationController
  def index
    @coins1 = Coin.order('RANDOM()').limit(3)
    @coins2 = Coin.order('RANDOM()').limit(3).offset(15)
    @coins3 = Coin.order('RANDOM()').limit(3).offset(35)
  end
end
