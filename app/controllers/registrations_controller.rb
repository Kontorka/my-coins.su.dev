class RegistrationsController < Devise::RegistrationsController
    add_breadcrumb 'Главная', :root_path
    add_breadcrumb 'Регистрация'
    respond_to :json
end
