class SessionsController < Devise::SessionsController
    add_breadcrumb 'Главная', :root_path
    add_breadcrumb 'Вход'
    respond_to :json
end
