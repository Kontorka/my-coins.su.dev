class ProfileController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  load_and_authorize_resource class: ProfileController
  add_breadcrumb 'Главная', :root_path

  def index
    add_breadcrumb "Профиль пользователя #{@profile.username}", profile_path
  end

  private

  def set_user
    @profile = current_user
  end

end
