json.extract! series, :id, :series_name, :published, :created_at, :updated_at
json.url series_url(series, format: :json)
