Rails.application.routes.draw do
  get 'profile', to: 'profile#index'

  resources :series
  get '/series_parse', to: 'series#parse'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  devise_for :users, :controllers => {sessions: 'sessions', registrations: 'registrations'}

  resources :users, path: '/user'

  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
